############################# Example of convolutional neural network 


# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# January 2023

#### Load libraries
import numpy as np
from matplotlib.pyplot import imshow, get_cmap,show,subplots
from scipy import signal
from PIL import Image

# Function to display images in 3 columns
# One picture on each axis
# Show() to display the image after being created
def displayImages(img1,img2,img3):
    _,axes = subplots(ncols=3) 
    axes[0].imshow(img1,cmap=get_cmap('gray')) 
    axes[1].imshow(img2,cmap=get_cmap('gray'))
    axes[2].imshow(img3,cmap=get_cmap('gray'))
    show()
    
# Function to display the image   
def displayImage(img,cmap=None):
    imshow(img,cmap)
    show()
    
    

# A picture is a matrix full of pixels, it's beatmap
image_test = np.array([
    [0,0,0,0,0], # each element corresponds to a pixel
    [0,1,1,1,0],
    [0,0,1,0,0],
    [0,0,0,0,0]])



# We create a convolution kernel
kernel = np.ones((3,3), np.float32)/2 # We force it to be of type float32 because we'll work with decimals
print(kernel) 
# np.ones create a 3*3 table with values divided by 2

# Display the images 
imgconvol = signal.convolve2d(image_test, kernel)
displayImages(image_test,kernel,imgconvol)
# On the left it's image_test created before
# In the middle we have our convolution filter (with computation of pixels we see the third picture on the right)

    
    
    
    
# Laplacian
# Open picture of a rabbit (drawing) 
rabbit1 = Image.open('C:/Users/mario/Downloads/rabbit1.png')
# Create kernel convolution
kernel_contour = np.array( [[0,1,0],
                            [1,-4,1],
                            [0,1,0]])

# Create and display image
imgconvol = signal.convolve2d(rabbit1, kernel_contour)
displayImages(rabbit1,kernel_contour,imgconvol)
# On the left there is our original picture, in the middle we use the filter and on the right we have the rabbit outline


# Open another picture of a rabbit
rabbit2 = Image.open('C:/Users/mario/Downloads/rabbit2.jpg').convert('L')

# Create and display image
imgconvol = signal.convolve2d(rabbit2,kernel_contour)
displayImages(rabbit2,kernel_contour,imgconvol)
# In the same way, we have our original picture on the left
# In the middle the filter we apply on the picture
# And on the right we have the rabbit outline

    
    
    
    
    
# Gaussian kernel
# Create convolution kernel
kernel_gaussien = np.array([[1,2,1],
                            [2,4,2],
                            [1,2,1]])/16

# Create and display image
imgconvol = signal.convolve2d(rabbit2,kernel_gaussien)
displayImages(rabbit2,kernel_gaussien,imgconvol)    
    
    

#Contrast
# Create convolution kernel
kernel_inccontrast = np.array(	[[0,0,0,0,0], 
						[0,0,-1,0,0], 
						[0,-1,5,-1,0], 
						[0,0,-1,0,0], 
						[0,0,0,0,0]])
# Create and display image
imgconvol = signal.convolve2d(rabbit2,kernel_inccontrast)
displayImages(rabbit2,kernel_inccontrast, imgconvol)



    
